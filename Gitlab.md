
* 查看所有镜像

    docker images

* 查看所有容器

    docker ps -a

* 下载

    docker pull gitlab/gitlab-ce
    
* 卸载

    docker rm -f gitlab

* 安装

    docker run -d -p 10443:443 -p 10080:80 -p 10022:22 --name gitlab --hostname www.mei.com --restart always gitlab/gitlab-ce
    
    docker run -d -p 10443:443 -p 10080:80 -p 10022:22 --name gitlab --hostname www.mei.com --restart always meixiaoliang/gitlab

* 管理员身份进入

    docker exec -it -u root gitlab bash

* 改时区

    echo 'Asia/Beijing' >/etc/timezone 
    
    echo 'Asia/Shanghai' >/etc/timezone
    
    cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && date

* 改项目`clone`地址及端口
    
    cd /opt/gitlab/embedded/service/gitlab-rails/config && vim gitlab.yml 
    
    gitlab-ctl restart  

    
* 汉化
    
    gitlab-ctl stop

    查看版本

    cat /opt/gitlab/embedded/service/gitlab-rails/VERSION
    
    下载汉化包，注意版本号要一致
    https://gitlab.com/xhang/gitlab
    
    cd /var && wget https://gitlab.com/xhang/gitlab/-/archive/11-10-stable-zh/gitlab-11-10-stable-zh.zip && unzip gitlab-11-10-stable-zh.zip
    
    cp -rf  /var/gitlab-11-10-stable-zh/*  /opt/gitlab/embedded/service/gitlab-rails/
    
    gitlab-ctl start
    
* 管理员密码（9 个 1）

    111111111