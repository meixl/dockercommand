* 下载

    docker pull nginx

* 卸载

    docker rm -f nginx

* 安装（初始化密码: root）

    docker run -d -p 31000:80 --restart always --name nginx nginx
    
* 启动

    docker start nginx

* 进入

    docker exec -it nginx bash
    
* 登录

    mysql -u root -p