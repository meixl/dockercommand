* 下载

    docker pull zookeeper

* 卸载

    docker rm -f zookeeper

* 安装

    docker run -d -p 31000:80 --restart always --name zookeeper zookeeper
    
    docker run -d -P --restart always --name zookeeper zookeeper
    
* 启动

    docker start zookeeper

* 进入

    docker exec -it zookeeper bash