* 查看所有镜像

    docker images

* 查看所有容器

    docker ps -a

* 下载

    docker pull redis

* 卸载

    docker rm -f redis

* 安装（初始化密码: redis）

    docker run -d -p 6379:6379 --name redis --restart always redis --requirepass "redis"

* 进入

    docker exec -it redis redis-cli

* 密码登录

    auth redis