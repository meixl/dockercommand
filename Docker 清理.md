* 查看所有镜像

    docker images

* 查看所有容器

    docker ps -a
    
* 查看磁盘使用情况

    docker system df

* 清理未使用的容器

    docker container prune 

* 清理未使用的容器及镜像（**搞不好就全删了，慎用**）

    docker system prune -a

* 删除指定容器

    docker rm 【容器名字】

* 删除指定镜像

    docker rmi 【镜像名字】