* 查看所有镜像

    docker images

* 查看所有容器

    docker ps -a

* 下载

    docker pull elasticsearch
    
* 卸载
    
    docker rm -f ela
    
* 安装

    docker run -d -p 9200:9200 -p 9300:9300 --name ela elasticsearch
    
    docker run -d -p 9200:9200 -p 9300:9300 --name ela meixiaoliang/elasticsearch

* 进入

    docker exec -it ela bash
    
* 安装 vim

    apt-get update
    
    apt-get install vim
    
* 编辑配置文件

    cd config
    
    vi elasticsearch.yml

* 配置文件内容（这里主要是为了安装 elasticsearch-head 插件）


    http:
        host: 0.0.0.0
        cors:
            enabled: true
            allow-origin: "*"
            
---

* head 安装

    docker run -d -p 9100:9100 --name  ela-head docker.io/mobz/elasticsearch-head:5

