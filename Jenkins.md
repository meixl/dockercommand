* 查看所有镜像

    docker images

* 查看所有容器

    docker ps -a

* 下载

    docker pull jenkins
    
* 卸载

    docker rm -f jenkins

* 安装
    
    docker run -d -p 9111:8080 -p 50000:50000 -p 10001:10001 -p 10002:10002 -p 10003:10003 -p 10004:10004 -p 10005:10005 -p 10006:10006 -p 10007:10007 -p 10008:10008 -p 10009:10009 -p 10010:10010 -p 10011:10011 -p 10012:10012 -p 10013:10013 -p 10014:10014 -p 10015:10015 -p 12001:22 --name jenkins jenkinsci/jenkins:latest

    docker run -d -p 9111:8080 -p 50000:50000 -p 10001:10001 -p 10002:10002 -p 10003:10003 -p 10004:10004 -p 10005:10005 -p 10006:10006 -p 10007:10007 -p 10008:10008 -p 10009:10009 -p 10010:10010 -p 10011:10011 -p 10012:10012 -p 10013:10013 -p 10014:10014 -p 10015:10015 -p 12001:22 --name jenkins meixiaoliang/jenkins
    
* 启动

    docker start jenkins

* 进入**请记录管理员密码**

    docker exec -it -u root jenkins bash
    
    cd var/jenkins_home/secrets/ && cat initialAdminPassword

* 改时区

    cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && date

* 密码

    home e9c2fe322e324f6abb59ac7ee34ade63
    
    hzry 7cd9b010ed904300b8b90c364f25d9cd
    
    company 0a473d51acf54b0893d8e0844ea7a8ff

* 插件安装

    maven: Maven Integration maven 构建项目必备
    
    tomcat: jar 包就不需要安装

* 环境

    java /docker-java-home
    
    git 不需要动
    
    maven 自己装一个，整个项目 jar 包拉取 10 分钟

* maven 脚本

    clean package -Dmaven.test.skip=true -P dev

* `jar`运行脚本

```sh
# 保留进程，这个配置参考：https://www.jianshu.com/p/f67d8e0f8092
BUILD_ID=dontKillMe

# 端口、项目构建后目录、项目构建后名称
PORT=10001
SERVICE_DIR=/var/jenkins_home/workspace/helloworld/target
SERVICE_NAME=helloworld-0.0.1-SNAPSHOT.jar

# 每次进程启动前，查询该进程是否已启动。若启动，则干掉
TEMP_PID=`ps -ef | grep -w "$SERVICE_NAME" | grep  "java" | awk '{print $2}'`
if [ "$TEMP_PID" != "" ]; then
   kill -9 $TEMP_PID
fi

# 跳转到构建后目录，指定端口启动 jar 包
cd $SERVICE_DIR
nohup java -jar $SERVICE_NAME --server.port=$PORT &
```
* tomcat 安装
 
    https://mirrors.cnnic.cn/apache/tomcat/

    wget https://mirrors.cnnic.cn/apache/tomcat/tomcat-8/v8.5.40/bin/apache-tomcat-8.5.40.tar.gz

* `静态页`运行脚本

```sh
BUILD_ID=dontKillMe

# 项目构建后目录、项目名
SERVICE_DIR=/var/jenkins_home/workspace
PROJECT_NAME=monitorSystem
# 【tomcat】目录、文件夹名
TOMCAT_LOCATE_DIR=/var
TOMCAT_NAME=tomcat_8.5_1

# 若【tomcat】启动，则关闭
TEMP_PID=`ps -ef | grep -w "$TOMCAT_NAME" | grep  "java" | awk '{print $2}'`
if [ "$TEMP_PID" != "" ]; then
   kill -9 $TEMP_PID
fi

# 若【tocmat】 webapps 目录下已存在代码，则删除该文件夹
cd $TOMCAT_LOCATE_DIR/
cd $TOMCAT_NAME/
cd webapps/
rm -rf $PROJECT_NAME

# git 拉取代码移动到【tocmat】 webapps 目录下
cp -r $SERVICE_DIR/$PROJECT_NAME $TOMCAT_LOCATE_DIR/$TOMCAT_NAME/webapps/

# 跳转到【tomcat】目录，并启动
cd ../bin/
./startup.sh
```

忘记Jenkins管理员密码的解决办法
[https://blog.csdn.net/jlminghui/article/details/54952148](https://blog.csdn.net/jlminghui/article/details/54952148)