* 查看所有镜像
```
  docker images
```


* 查看所有容器
```
  docker ps -a
```

* 下载
```
  docker pull mysql:5.6
    
  docker pull mysql:5.7

  docker pull mysql
```

* 卸载
```
  docker rm -f mysql
    
  docker rm -f mysql_5.6
   
  docker rm -f mysql_5.7
```

* 安装（初始化密码: root）
```
  docker run -d -p 3306:3306 --restart always -e MYSQL_ROOT_PASSWORD=root --name mysql mysql:5.6
    
  docker run -d -p 3306:3306 --restart always -e MYSQL_ROOT_PASSWORD=root --name mysql_5.7 mysql:5.7

  docker run -d -p 3312:3306 --restart always -e MYSQL_ROOT_PASSWORD=root --name mysql_8 mysql --lower_case_table_names=1
``` 

* 启动
```
  docker start mysql
```

* 进入
```
  docker exec -it mysql bash
    
  docker exec -it mysql_5.7 bash
```    

* 登录
```
  mysql -u root -p
```