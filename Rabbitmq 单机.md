* 查看所有镜像
```
  docker images
```

* 查看所有容器
```
  docker ps -a
```

* 下载
```
  docker pull rabbitmq:management
```

* 卸载
```
  docker rm -f rabbitmq:management
```

* 安装
```
  docker run -d -p 5672:5672 -p 15672:15672 --restart always --name rabbitmq rabbitmq:management
``` 

* 启动
```
  docker start rabbitmq
```

